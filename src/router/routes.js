/**
 * Created by baidm in 2022/5/10 on 21:27
 */
export default [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/Home.vue")
  },
  {
    path: "/limit-map-range",
    name: "LimitMapRange",
    component: () => import("@/components/LimitMapRange.component.vue")
  },
  {
    path: "/map-zoom",
    name: "MapZoom",
    component: () => import("@/components/MapZoom.component.vue")
  },
  {
    path: "/map-revolve",
    name: "MapRevolve",
    component: () => import("@/components/MapRevolve.component.vue")
  },
  {
    path: "/symbol-fly",
    name: "SymbolFly",
    component: () => import("@/components/SymbolFly.component.vue")
  },
  {
    path: "/map-fly",
    name: "MapFly",
    component: () => import("@/components/MapFly.component.vue")
  },
  {
    path: "/map-fly-play",
    name: "MapFlyPlay",
    component: () => import("@/components/MapFlyPlay.component.vue")
  },
  {
    path: "/map-fly-scroll",
    name: "MapFlyScroll",
    component: () => import("@/components/MapFlyScroll.component.vue")
  },
  {
    path: "/map-jump",
    name: "MapJump",
    component: () => import("@/components/MapJump.component.vue")
  },
  {
    path: "/map-full-screen",
    name: "MapFullScreen",
    component: () => import("@/components/MapFullScreen.component.vue")
  },
  {
    path: "/change-map-style",
    name: "ChangeMapStyle",
    component: () => import("@/components/ChangeMapStyle.component.vue")
  },
  {
    path: "/change-symbol-position",
    name: "ChangeSymbolPosition",
    component: () => import("@/components/ChangeSymbolPosition.component.vue")
  },
  {
    path: "/change-build-style",
    name: "ChangeBuildStyle",
    component: () => import("@/components/ChangeBuildStyle.component.vue")
  },
  {
    path: "/change-level-style",
    name: "ChangeLevelStyle",
    component: () => import("@/components/ChangeLevelStyle.component.vue")
  },
  {
    path: "/add-track-play",
    name: "AddTrackPlay",
    component: () => import("@/components/AddTrackPlay.component.vue")
  },
  {
    path: "/add-custom-point",
    name: "AddCustomPoint",
    component: () => import("@/components/AddCustomPoint.component.vue")
  },
  {
    path: "/add-custom-symbol",
    name: "AddCustomSymbol",
    component: () => import("@/components/AddCustomSymbol.component.vue")
  },
  {
    path: "/add-custom-symbol-popper",
    name: "AddCustomSymbolPopper",
    component: () => import("@/components/AddCustomSymbolPopper.component.vue")
  },
  {
    path: "/add-custom-symbol-click-popper",
    name: "AddCustomSymbolClickPopper",
    component: () => import("@/components/AddCustomSymbolClickPopper.component.vue")
  },
  {
    path: "/add-custom-symbol-hover-popper",
    name: "AddCustomSymbolHoverPopper",
    component: () => import("@/components/AddCustomSymbolHoverPopper.component.vue")
  },
  {
    path: "/add-circle-point",
    name: "AddCirclePoint",
    component: () => import("@/components/AddCirclePoint.component.vue")
  },
  {
    path: "/add-drag-point",
    name: "AddDragPoint",
    component: () => import("@/components/AddDragPoint.component.vue")
  },
  {
    path: "/add-marker",
    name: "AddMarker",
    component: () => import("@/components/AddMarker.component.vue")
  },
  {
    path: "/add-drag-marker",
    name: "AddDragMarker",
    component: () => import("@/components/AddDragMarker.component.vue")
  },
  {
    path: "/add-icon-point",
    name: "AddIconPoint",
    component: () => import("@/components/AddIconPoint.component.vue")
  },
  {
    path: "/add-line",
    name: "AddLine",
    component: () => import("@/components/AddLine.component.vue")
  },
  {
    path: "/add-polygon",
    name: "AddPolygon",
    component: () => import("@/components/AddPolygon.component.vue")
  },
  {
    path: "/add-massive-point",
    name: "AddMassivePoint",
    component: () => import("@/components/AddMassivePoint.component.vue")
  },
  {
    path: "/add-massive-line",
    name: "AddMassiveLine",
    component: () => import("@/components/AddMassiveLine.component.vue")
  },
  {
    path: "/add-gradient-line",
    name: "AddGradientLine",
    component: () => import("@/components/AddGradientLine.component.vue")
  },
  {
    path: "/add-area",
    name: "AddArea",
    component: () => import("@/components/AddArea.component.vue")
  },
  {
    path: "/add-heat-map",
    name: "AddHeatMap",
    component: () => import("@/components/AddHeatMap.component.vue")
  },
  {
    path: "/add-cluster",
    name: "AddCluster",
    component: () => import("@/components/AddCluster.component.vue")
  },
  {
    path: "/add-html-cluster",
    name: "AddHTMLCluster",
    component: () => import("@/components/AddHTMLCluster.component.vue")
  },
  {
    path: "/add-animate-point",
    name: "AddAnimatePoint",
    component: () => import("@/components/AddAnimatePoint.component.vue")
  },
  {
    path: "/add-animate-marker",
    name: "AddAnimateMarker",
    component: () => import("@/components/AddAnimateMarker.component.vue")
  },
  {
    path: "/add-animate-line",
    name: "AddAnimateLine",
    component: () => import("@/components/AddAnimateLine.component.vue")
  },
  {
    path: "/add-animate-image",
    name: "AddAnimateImage",
    component: () => import("@/components/AddAnimateImage.component.vue")
  },
  {
    path: "/add-animate-building",
    name: "AddAnimateBuilding",
    component: () => import("@/components/AddAnimateBuilding.component.vue")
  },
  {
    path: "/add-3d-building",
    name: "Add3DBuilding",
    component: () => import("@/components/Add3DBuilding.component.vue")
  },
  {
    path: "/add-3d-room",
    name: "Add3DRoom",
    component: () => import("@/components/Add3DRoom.component.vue")
  },
  {
    path: "/add-3d-model",
    name: "Add3DModel",
    component: () => import("@/components/Add3DModel.component.vue")
  },
  {
    path: "/add-video-play",
    name: "AddVideoPlay",
    component: () => import("@/components/AddVideoPlay.component.vue")
  },
  {
    path: "/add-hover",
    name: "AddHover",
    component: () => import("@/components/AddHover.component.vue")
  },
  {
    path: "/add-hover-filter",
    name: "AddHoverFilter",
    component: () => import("@/components/AddHoverFilter.component.vue")
  },
  {
    path: "/switch-filter",
    name: "SwitchFilter",
    component: () => import("@/components/SwitchFilter.component.vue")
  },
  {
    path: "/show-map-property",
    name: "ShowMapProperty",
    component: () => import("@/components/ShowMapProperty.component.vue")
  },
  {
    path: "/view-person",
    name: "ViewPerson",
    component: () => import("@/components/ViewPerson.component.vue")
  },
  {
    path: "/map-navigation",
    name: "MapNavigation",
    component: () => import("@/components/MapNavigation.component.vue")
  },
  {
    path: "/user-draw-polygon",
    name: "UserDrawPolygon",
    component: () => import("@/components/UserDrawPolygon.component.vue")
  },
  {
    path: "/user-query-addr",
    name: "UserQueryAddr",
    component: () => import("@/components/UserQueryAddr.component.vue")
  },
]
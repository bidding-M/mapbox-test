/**
 * Created by baidm in 2021/7/18 on 16:54
 */
import {createRouter, createWebHashHistory} from 'vue-router'
import routes from "./routes"

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
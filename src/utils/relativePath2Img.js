/**
 * Created by baidm in 2022/5/14 on 11:21
 */
export default function (path) {
  return new URL(`/src/assets/images/${path}`, import.meta.url).href;
}
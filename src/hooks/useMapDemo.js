/**
 * Created by baidm in 2022/5/5 on 21:55
 */
import MapLayerConst from "@/consts/MapLayerConst"
import {ref} from "vue"
import {openWindow} from "@/hooks/useCommon"
import {useRouter} from "vue-router";

export default function () {
  const router = useRouter()

  let menuStatusRef = ref(true);
  let mapDemoListRef = ref([
    {name: "[基础] 限制地图平移范围", id: MapLayerConst.LIMIT_RANGE_LAYER_ID},
    {name: "[基础] 地图缩放至边界框内", id: MapLayerConst.MAP_ZOOM_LAYER_ID},
    {name: "[基础] 地图环绕一点运动", id: MapLayerConst.MAP_REVOLVE_LAYER_ID},
    {name: "[基础] 飞机飞行", id: MapLayerConst.SYMBOL_FLY_LAYER_ID},
    {name: "[基础] 地图飞行", id: MapLayerConst.MAP_FLY_LAYER_ID},
    {name: "[基础] 幻灯片播放地图位置", id: MapLayerConst.MAP_FLY_PLAY_LAYER_ID},
    {name: "[基础] 根据滚动位置飞到某处", id: MapLayerConst.MAP_FLY_SCROLL_LAYER_ID},
    {name: "[基础] 跳至一系列地点", id: MapLayerConst.MAP_JUMP_LAYER_ID},
    {name: "[基础] 全屏查看地图", id: MapLayerConst.MAP_FULL_SCREEN_LAYER_ID},
    {name: "[改变] 更新地图的样式", id: MapLayerConst.CHANGE_MAP_STYLE_ID},
    {name: "[改变] 更新建筑物的样式", id: MapLayerConst.CHANGE_BUILD_STYLE_ID},
    {name: "[改变] 更新等值线图层", id: MapLayerConst.CHANGE_LEVEL_STYLE_ID},
    {name: "[改变] 实时数据更新图标的位置", id: MapLayerConst.CHANGE_SYMBOL_LAYER_ID},
    {name: "[改变] 轨迹播放效果", id: MapLayerConst.TRACK_LAYER_ID},
    {name: "[添加] 动画图标", id: MapLayerConst.ANIMATE_SYMBOL_LAYER_ID},
    {name: "[添加] 自定义图标", id: MapLayerConst.CUSTOM_SYMBOL_LAYER_ID},
    {name: "[添加] 自定义图标显示popper", id: MapLayerConst.CUSTOM_SYMBOL_POPPER_LAYER_ID},
    {name: "[添加] 自定义图标click显示弹出窗", id: MapLayerConst.CUSTOM_SYMBOL_CLICK_POPPER_LAYER_ID},
    {name: "[添加] 自定义图标hover显示弹出窗", id: MapLayerConst.CUSTOM_SYMBOL_Hover_POPPER_LAYER_ID},
    {name: "[添加] 点", id: MapLayerConst.POINT_LAYER_ID},
    {name: "[添加] 可拖动的点", id: MapLayerConst.DRAG_POINT_LAYER_ID},
    {name: "[添加] marker", id: MapLayerConst.MARKER_LAYER_ID},
    {name: "[添加] 可拖动的marker", id: MapLayerConst.DRAG_MARKER_LAYER_ID},
    {name: "[添加] GeoJSON 点", id: MapLayerConst.ICON_POINT_LAYER_ID},
    {name: "[添加] GeoJSON 线", id: MapLayerConst.LINE_LAYER_ID},
    {name: "[添加] GeoJSON 多边形", id: MapLayerConst.POLYGON_LAYER_ID},
    {name: "[添加] 海量撒点", id: MapLayerConst.MASSIVE_POINT_LAYER_ID},
    {name: "[添加] 海量画线", id: MapLayerConst.MASSIVE_LINE_LAYER_ID},
    {name: "[添加] 渐变色线条", id: MapLayerConst.LINE_GRADIENT_LAYER_ID},
    {name: "[添加] 区域", id: MapLayerConst.AREA_LAYER_ID},
    {name: "[添加] 热力图图层", id: MapLayerConst.HEAT_LAYER_ID},
    {name: "[添加] 样式聚类", id: MapLayerConst.CLUSTER_LAYER_ID},
    {name: "[添加] HTML聚类", id: MapLayerConst.CLUSTER_HTML_LAYER_ID},
    {name: "[添加] 点动画效果", id: MapLayerConst.ANIMATE_POINT_LAYER_ID},
    {name: "[添加] marker动画效果", id: MapLayerConst.ANIMATE_MARKER_LAYER_ID},
    {name: "[添加] 线动画效果", id: MapLayerConst.ANIMATE_LINE_LAYER_ID},
    {name: "[添加] 图像动画效果", id: MapLayerConst.ANIMATE_IMAGE_LAYER_ID},
    {name: "[添加] 3D建筑物动画效果", id: MapLayerConst.ANIMATE_BUILDING_LAYER_ID},
    {name: "[添加] 3D建筑物", id: MapLayerConst.BUILDINGS_LAYER_ID},
    {name: "[添加] 3D室内地图", id: MapLayerConst.ROOM_BUILDINGS_LAYER_ID},
    {name: "[添加] 3D模型", id: MapLayerConst.MODEL_LAYER_ID},
    {name: "[添加] 视频播放图层", id: MapLayerConst.VIDEO_LAYER_ID},
    {name: "[添加] 鼠标悬停效果", id: MapLayerConst.HOVER_LAYER_ID},
    {name: "[添加] 高亮包含相似数据的部分", id: MapLayerConst.HOVER_FILTER_LAYER_ID},
    {name: "[获取] 切换列表筛选符号", id: MapLayerConst.SWITCH_FILTER_LAYER_ID},
    {name: "[获取] 地图元素的属性", id: MapLayerConst.HOVER_SHOW_ENTRY_PROPERTY},
    {name: "[综合] 可视化人口密度", id: MapLayerConst.VIEW_PERSON_LAYER_ID},
    {name: "[综合] 地图导航驾驶方向", id: MapLayerConst.MAP_NAVIGATION_LAYER_ID},
    {name: "[交互] 绘制多边形", id: MapLayerConst.USER_DRAW_POLYGON_LAYER_ID},
    {name: "[交互] 位置搜索", id: MapLayerConst.USER_QUERY_ADDR_LAYER_ID},
  ]);

  //切换展示demo
  const toggleMapDemo = (menu) => {
    let findRes = mapDemoListRef.value.find(item => item.id === menu.id)
    switch (menu.id) {
      case MapLayerConst.LIMIT_RANGE_LAYER_ID:
        openWindow(router, "LimitMapRange", findRes.name)
        break;
      case MapLayerConst.MAP_ZOOM_LAYER_ID:
        openWindow(router, "MapZoom", findRes.name)
        break;
      case MapLayerConst.MAP_REVOLVE_LAYER_ID:
        openWindow(router, "MapRevolve", findRes.name)
        break;
      case MapLayerConst.SYMBOL_FLY_LAYER_ID:
        openWindow(router, "SymbolFly", findRes.name)
        break;
      case MapLayerConst.MAP_FLY_LAYER_ID:
        openWindow(router, "MapFly", findRes.name)
        break;
      case MapLayerConst.MAP_FLY_PLAY_LAYER_ID:
        openWindow(router, "MapFlyPlay", findRes.name)
        break;
      case MapLayerConst.MAP_FLY_SCROLL_LAYER_ID:
        openWindow(router, "MapFlyScroll", findRes.name)
        break;
      case MapLayerConst.MAP_JUMP_LAYER_ID:
        openWindow(router, "MapJump", findRes.name)
        break;
      case MapLayerConst.MAP_FULL_SCREEN_LAYER_ID:
        openWindow(router, "MapFullScreen", findRes.name)
        break;
      case MapLayerConst.CHANGE_MAP_STYLE_ID:
        openWindow(router, "ChangeMapStyle", findRes.name)
        break;
      case MapLayerConst.CHANGE_BUILD_STYLE_ID:
        openWindow(router, "ChangeBuildStyle", findRes.name)
        break;
      case MapLayerConst.CHANGE_LEVEL_STYLE_ID:
        openWindow(router, "ChangeLevelStyle", findRes.name)
        break;
      case MapLayerConst.CHANGE_SYMBOL_LAYER_ID:
        openWindow(router, "ChangeSymbolPosition", findRes.name)
        break;
      case MapLayerConst.TRACK_LAYER_ID:
        openWindow(router, "AddTrackPlay", findRes.name)
        break;
      case MapLayerConst.ANIMATE_SYMBOL_LAYER_ID:
        openWindow(router, "AddCustomPoint", findRes.name)
        break;
      case MapLayerConst.CUSTOM_SYMBOL_LAYER_ID:
        openWindow(router, "AddCustomSymbol", findRes.name)
        break;
      case MapLayerConst.CUSTOM_SYMBOL_POPPER_LAYER_ID:
        openWindow(router, "AddCustomSymbolPopper", findRes.name)
        break;
      case MapLayerConst.CUSTOM_SYMBOL_CLICK_POPPER_LAYER_ID:
        openWindow(router, "AddCustomSymbolClickPopper", findRes.name)
        break;
      case MapLayerConst.CUSTOM_SYMBOL_Hover_POPPER_LAYER_ID:
        openWindow(router, "AddCustomSymbolHoverPopper", findRes.name)
        break;
      case MapLayerConst.POINT_LAYER_ID:
        openWindow(router, "AddCirclePoint", findRes.name)
        break;
      case MapLayerConst.DRAG_POINT_LAYER_ID:
        openWindow(router, "AddDragPoint", findRes.name)
        break;
      case MapLayerConst.MARKER_LAYER_ID:
        openWindow(router, "AddMarker", findRes.name)
        break;
      case MapLayerConst.DRAG_MARKER_LAYER_ID:
        openWindow(router, "AddDragMarker", findRes.name)
        break;
      case MapLayerConst.ICON_POINT_LAYER_ID:
        openWindow(router, "AddIconPoint", findRes.name)
        break;
      case MapLayerConst.LINE_LAYER_ID:
        openWindow(router, "AddLine", findRes.name)
        break;
      case MapLayerConst.POLYGON_LAYER_ID:
        openWindow(router, "AddPolygon", findRes.name)
        break;
      case MapLayerConst.MASSIVE_POINT_LAYER_ID:
        openWindow(router, "AddMassivePoint", findRes.name)
        break;
      case MapLayerConst.MASSIVE_LINE_LAYER_ID:
        openWindow(router, "AddMassiveLine", findRes.name)
        break;
      case MapLayerConst.LINE_GRADIENT_LAYER_ID:
        openWindow(router, "AddGradientLine", findRes.name)
        break;
      case MapLayerConst.AREA_LAYER_ID:
        openWindow(router, "AddArea", findRes.name)
        break;
      case MapLayerConst.HEAT_LAYER_ID:
        openWindow(router, "AddHeatMap", findRes.name)
        break;
      case MapLayerConst.CLUSTER_LAYER_ID:
        openWindow(router, "AddCluster", findRes.name)
        break;
      case MapLayerConst.CLUSTER_HTML_LAYER_ID:
        openWindow(router, "AddHTMLCluster", findRes.name)
        break;
      case MapLayerConst.ANIMATE_POINT_LAYER_ID:
        openWindow(router, "AddAnimatePoint", findRes.name)
        break;
      case MapLayerConst.ANIMATE_MARKER_LAYER_ID:
        openWindow(router, "AddAnimateMarker", findRes.name)
        break;
      case MapLayerConst.ANIMATE_LINE_LAYER_ID:
        openWindow(router, "AddAnimateLine", findRes.name)
        break;
      case MapLayerConst.ANIMATE_IMAGE_LAYER_ID:
        openWindow(router, "AddAnimateImage", findRes.name)
        break;
      case MapLayerConst.ANIMATE_BUILDING_LAYER_ID:
        openWindow(router, "AddAnimateBuilding", findRes.name)
        break;
      case MapLayerConst.BUILDINGS_LAYER_ID:
        openWindow(router, "Add3DBuilding", findRes.name)
        break;
      case MapLayerConst.ROOM_BUILDINGS_LAYER_ID:
        openWindow(router, "Add3DRoom", findRes.name)
        break;
      case MapLayerConst.MODEL_LAYER_ID:
        openWindow(router, "Add3DModel", findRes.name)
        break;
      case MapLayerConst.VIDEO_LAYER_ID:
        openWindow(router, "AddVideoPlay", findRes.name)
        break;
      case MapLayerConst.HOVER_LAYER_ID:
        openWindow(router, "AddHover", findRes.name)
        break;
      case MapLayerConst.HOVER_FILTER_LAYER_ID:
        openWindow(router, "AddHoverFilter", findRes.name)
        break;
      case MapLayerConst.SWITCH_FILTER_LAYER_ID:
        openWindow(router, "SwitchFilter", findRes.name)
        break;
      case MapLayerConst.HOVER_SHOW_ENTRY_PROPERTY:
        openWindow(router, "ShowMapProperty", findRes.name)
        break;
      case MapLayerConst.VIEW_PERSON_LAYER_ID:
        openWindow(router, "ViewPerson", findRes.name)
        break;
      case MapLayerConst.MAP_NAVIGATION_LAYER_ID:
        openWindow(router, "MapNavigation", findRes.name)
        break;
      case MapLayerConst.USER_DRAW_POLYGON_LAYER_ID:
        openWindow(router, "UserDrawPolygon", findRes.name)
        break;
      case MapLayerConst.USER_QUERY_ADDR_LAYER_ID:
        openWindow(router, "UserQueryAddr", findRes.name)
        break;
    }
  }

  return {
    menuStatusRef, mapDemoListRef, toggleMapDemo
  }
}
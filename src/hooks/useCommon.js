/**
 * Created by baidm in 2022/5/11 on 20:46
 */

export const openWindow = (router, name, title) => {
  let {origin, pathname} = window.location
  let {href} = router.resolve({name})
  let windowDocument = window.open(`${origin}${pathname}${href}`, "_blank")
  setTimeout(() => {
    windowDocument.document.title = `${title}-${document.title}`
  }, 300)
}
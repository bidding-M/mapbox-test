/**
 * Created by baidm in 2022/5/15 on 16:17
 */
import HttpCommonApiService from "./HttpCommonApi.service";

class MapboxAssetsService {
  constructor() {
    this.$http = new HttpCommonApiService({
      baseURL: `${window.location.origin}/mapbox-test/libs/mapbox`
    })
  }

  queryAssets(filename) {
    return this.$http.get(`/assets/${filename}`)
  }

  queryStyle(filename) {
    return this.$http.get(`/style/${filename}`)
  }
}

export default new MapboxAssetsService()
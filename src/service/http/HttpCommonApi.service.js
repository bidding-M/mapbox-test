/**
 * Created by 86185 in 2022/5/15 on 10:34
 */
import axios from "axios"

class HttpCommonApiService {
  constructor({baseURL, url}) {
    this.instance = axios.create({
      baseURL: baseURL,
      headers: {
        "Content-Type": "application/json"
      },

      transformRequest: [function (data, headers) {
        return data;
      }],

      transformResponse: [function (data) {
        return JSON.parse(data);
      }]
    })

  }

  get(url) {
    return this.instance.get(url);
  }

  post(url, params) {
    return this.instance.post(url, JSON.stringify(params));
  }
}

export default HttpCommonApiService;

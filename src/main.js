import {createApp} from "vue"
import App from "@/App.vue"
import router from "@/router/index"
import store from "@/store/index"

import "font-awesome/css/font-awesome.min.css"

import ElementPlus from "element-plus"
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

createApp(App)
  .use(router)
  .use(store)
  .use(ElementPlus, {locale: zhCn})
  .mount("#app");

/**
 * Created by baidm in 2021/7/18 on 15:13
 */
const CHANGE_MAP_STYLE_ID = "change_map_style_id"
const CHANGE_BUILD_STYLE_ID = "change_build_style_id"
const CHANGE_LEVEL_STYLE_ID = "change_level_style_id"
const ANIMATE_SYMBOL_LAYER_ID = "animate_symbol_layer_id";
const CUSTOM_SYMBOL_LAYER_ID = "custom_symbol_layer_id"
const CUSTOM_SYMBOL_POPPER_LAYER_ID = "custom_symbol_popper_layer_id"
const ICON_POINT_LAYER_ID = "icon_point_layer_id";
const POINT_LAYER_ID = "point_layer_id";
const DRAG_POINT_LAYER_ID = "drag_point_layer_id"
const MARKER_LAYER_ID = "marker_layer_id";
const DRAG_MARKER_LAYER_ID = "drag_marker_layer_id"
const LINE_LAYER_ID = "line_layer_id";
const LINE_GRADIENT_LAYER_ID = "line_gradient_layer_id";
const AREA_LAYER_ID = "area_layer_id";
const POLYGON_LAYER_ID = "polygon_layer_id";
const HEAT_LAYER_ID = "heat_layer_id";
const HEAT_POINT_LAYER_ID = "heat_point_layer_id";
const CLUSTER_LAYER_ID = "cluster_layer_id";
const CLUSTER_HTML_LAYER_ID = "cluster_html_layer_id"
const BUILDINGS_LAYER_ID = "buildings_layer_id";
const ROOM_BUILDINGS_LAYER_ID = "room_building_layer_id";
const MODEL_LAYER_ID = "model_layer_id";
const VIDEO_LAYER_ID = "video_layer_id";
const ANIMATE_POINT_LAYER_ID = "animate_point_layer_id"
const ANIMATE_MARKER_LAYER_ID = "animate_marker_layer_id"
const ANIMATE_LINE_LAYER_ID = "animate_line_layer_id"
const ANIMATE_IMAGE_LAYER_ID = "animate_image_layer_id";
const ANIMATE_BUILDING_LAYER_ID = "animate_building_layer_id"
const MASSIVE_POINT_LAYER_ID = "massive_point_layer_id"
const MASSIVE_LINE_LAYER_ID = "massive_line_layer_id"
const TRACK_LAYER_ID = "track_layer_id";
const CHANGE_SYMBOL_LAYER_ID = "change_icon_layer_id"
const HOVER_LAYER_ID = "hover_layer_id";
const HOVER_FILTER_LAYER_ID = "hover_filter_layer_id";
const SWITCH_FILTER_LAYER_ID = "switch_filter_layer_id"
const HOVER_SHOW_ENTRY_PROPERTY = "hover_show_entry_property";
const LIMIT_RANGE_LAYER_ID = "limit_range_layer_id"
const MAP_ZOOM_LAYER_ID = "map_zoom_layer_id"
const MAP_REVOLVE_LAYER_ID = "mao_revolve_layer_id"
const SYMBOL_FLY_LAYER_ID = "symbol_fly_layer_id"
const MAP_FLY_LAYER_ID = "map_fly_layer_id"
const MAP_FLY_PLAY_LAYER_ID = "map_fly_play_layer_id"
const MAP_FLY_SCROLL_LAYER_ID = "map_fly_scroll_layer_id"
const MAP_JUMP_LAYER_ID = "map_jump_layer_id"
const MAP_FULL_SCREEN_LAYER_ID = "map_full_screen_layer_id"
const VIEW_PERSON_LAYER_ID = "view_person_layer_id"
const MAP_NAVIGATION_LAYER_ID = "map_navigation_layer_id"
const USER_DRAW_POLYGON_LAYER_ID = "user_draw_polygon_layer_id"
const USER_QUERY_ADDR_LAYER_ID = "user_query_addr_layer_id"
const CUSTOM_SYMBOL_CLICK_POPPER_LAYER_ID = "custom_symbol_click_popper_layer_id"
const CUSTOM_SYMBOL_Hover_POPPER_LAYER_ID = "custom_symbol_hover_popper_layer_id"

export default {
  CHANGE_MAP_STYLE_ID,
  CHANGE_BUILD_STYLE_ID,
  CHANGE_LEVEL_STYLE_ID,
  ANIMATE_SYMBOL_LAYER_ID,
  CUSTOM_SYMBOL_LAYER_ID,
  CUSTOM_SYMBOL_POPPER_LAYER_ID,
  ICON_POINT_LAYER_ID,
  POINT_LAYER_ID,
  DRAG_POINT_LAYER_ID,
  MARKER_LAYER_ID,
  DRAG_MARKER_LAYER_ID,
  LINE_LAYER_ID,
  LINE_GRADIENT_LAYER_ID,
  AREA_LAYER_ID,
  POLYGON_LAYER_ID,
  HEAT_LAYER_ID,
  HEAT_POINT_LAYER_ID,
  CLUSTER_LAYER_ID,
  CLUSTER_HTML_LAYER_ID,
  BUILDINGS_LAYER_ID,
  ROOM_BUILDINGS_LAYER_ID,
  MODEL_LAYER_ID,
  VIDEO_LAYER_ID,
  ANIMATE_POINT_LAYER_ID,
  ANIMATE_MARKER_LAYER_ID,
  ANIMATE_LINE_LAYER_ID,
  ANIMATE_IMAGE_LAYER_ID,
  ANIMATE_BUILDING_LAYER_ID,
  MASSIVE_POINT_LAYER_ID,
  MASSIVE_LINE_LAYER_ID,
  TRACK_LAYER_ID,
  CHANGE_SYMBOL_LAYER_ID,
  HOVER_LAYER_ID,
  HOVER_FILTER_LAYER_ID,
  SWITCH_FILTER_LAYER_ID,
  HOVER_SHOW_ENTRY_PROPERTY,
  LIMIT_RANGE_LAYER_ID,
  MAP_ZOOM_LAYER_ID,
  MAP_REVOLVE_LAYER_ID,
  SYMBOL_FLY_LAYER_ID,
  MAP_FLY_LAYER_ID,
  MAP_FLY_PLAY_LAYER_ID,
  MAP_FLY_SCROLL_LAYER_ID,
  MAP_JUMP_LAYER_ID,
  MAP_FULL_SCREEN_LAYER_ID,
  VIEW_PERSON_LAYER_ID,
  MAP_NAVIGATION_LAYER_ID,
  USER_DRAW_POLYGON_LAYER_ID,
  USER_QUERY_ADDR_LAYER_ID,
  CUSTOM_SYMBOL_CLICK_POPPER_LAYER_ID,
  CUSTOM_SYMBOL_Hover_POPPER_LAYER_ID
}
/**
 * Created by baidm in 2021/7/17 on 18:16
 */
// 地图访问令牌
// export const accessToken = "pk.eyJ1IjoibWFvcmV5IiwiYSI6ImNqNWhrenIwcDFvbXUyd3I2bTJxYzZ4em8ifQ.KHZIehQuWW9AsMaGtATdwA";
export const accessToken = "pk.eyJ1IjoibHVrYXNtYXJ0aW5lbGxpIiwiYSI6ImNpem85dmhwazAyajIyd284dGxhN2VxYnYifQ.HQCmyhEXZUTz3S98FMrVAQ";

// 地图图层样式
export const mapStyle = {
  "light-v10": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-light-v10.json`,
  "dark-v10": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-dark-v10.json`,
  "streets-v11": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-streets-v11.json`,
  "streets-v11?optimize=true": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-streets-v11-optimize-true.json`,
  "outdoors-v11": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-outdoors-v11.json`,
  "satellite-v9": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-satellite-v9.json`,
  "satellite-streets-v10": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-satellite-streets-v10.json`,
  "navigation-preview-day-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-navigation-preview-day-v2.json`,
  "navigation-preview-night-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-navigation-preview-night-v2.json`,
  "navigation-guidance-day-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-navigation-guidance-day-v2.json`,
  "navigation-guidance-night-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-navigation-guidance-night-v2.json`,
  "traffic-day-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-traffic-day-v2.json`,
  "traffic-night-v2": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-traffic-night-v2.json`,
  "custom": `${window.location.origin}/mapbox-test/libs/mapbox/style/style-custom.json`
};

// 地图中心经纬度
export const mapCerter = [106.551556, 29.563009];

// 地图层级
export const mapZoom = 12;

// 地图初始化时的方位角（旋转角度），以正北方的逆时针转动度数计量
export const mapBearing = 0;

// 地图初始化时的倾角，按偏离屏幕水平面的度数计量（0-60）
export const mapPitch = 0;
import {defineConfig} from "vite"
import vue from "@vitejs/plugin-vue"

// https://vitejs.dev/config/
export default defineConfig({
  base: "/mapbox-test/",
  plugins: [vue()],
  resolve: {
    alias: {
      "@": "/src"
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        // 全局引入公共样式文件，多个样式文件引入：
        // `@import "@/common/css/common1.style.scss";@import "@/common/css/common2.style.scss";`
        additionalData: `@import "@/common/css/base.style.scss";@import "@/common/css/common.style.scss";`
      }
    }
  },
  server: {
    port: 10010,
    hmr: true,
    proxy: {
      "/api": {
        target: "http://jsonplaceholder.typicode.com",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, "")
      },
    }
  }
})

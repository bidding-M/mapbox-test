# mapbox-test

[在线演示](https://bidding-m.gitee.io/mapbox-test)

#### 安装教程

1.  download mapbox-test
2.  npm install
3.  npm run dev


#### 特技

*、[博客地址](https://blog.csdn.net/LiyangBai/article/details/119514504?spm=1001.2014.3001.5501)
1、[行政边界查询地址](https://lbs.amap.com/demo/javascript-api/example/district-search/draw-district-boundaries)
2、[坐标拾取系统地址](http://api.map.baidu.com/lbsapi/getpoint/)
